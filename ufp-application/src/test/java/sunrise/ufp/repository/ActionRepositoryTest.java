package sunrise.ufp.repository;

import org.hamcrest.collection.IsEmptyCollection;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import sunrise.ufp.model.UfpAction;

import java.util.List;

import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = "ufp.jobs.enabled=false")
@Transactional(transactionManager = "ufpTransactionManager")
@ComponentScan({"sunrise.ufp"})
public class ActionRepositoryTest {

    @Autowired
    private ActionRepository actionRepository;

    @Test
    public void findAllTest() {
        List<UfpAction> ufpActions = actionRepository.findAll();

        assertThat(ufpActions, not(IsEmptyCollection.empty()));
    }

}
