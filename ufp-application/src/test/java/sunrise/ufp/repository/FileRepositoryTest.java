package sunrise.ufp.repository;

import org.hamcrest.collection.IsEmptyCollection;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import sunrise.ufp.model.FileProcessingStatus;
import sunrise.ufp.model.UfpExecution;
import sunrise.ufp.model.UfpFile;

import java.sql.Date;
import java.time.Instant;
import java.util.List;

import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = "ufp.jobs.enabled=false")
@Transactional(transactionManager = "ufpTransactionManager")
public class FileRepositoryTest {

    @Autowired
    private FileRepository fileRepository;

    @Test
    public void findAllTest() {
        List<UfpFile> ufpFiles = fileRepository.findAll();

        assertThat(ufpFiles, not(IsEmptyCollection.empty()));
    }

    @Test
    public void saveFileTest() {
        UfpFile file = fileRepository.findById(1L).orElse(null);
        assertNotNull(file);

        UfpExecution execution = UfpExecution.builder()
                .ufpFile(file)
                .ufpAction(file.getUfpUpload().getAction())
                .tryCounter(1)
                .manualRetry(Boolean.FALSE)
                .status(FileProcessingStatus.SUCCESS)
                .date(Date.from(Instant.now()))
                .build();

        file.addUfpExecution(execution);

        UfpFile savedFile = fileRepository.save(file);
        assertNotNull(savedFile);
    }

}
