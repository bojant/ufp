package sunrise.ufp.repository;

import org.hamcrest.collection.IsEmptyCollection;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import sunrise.ufp.model.ActionName;
import sunrise.ufp.model.UfpUpload;
import sunrise.ufp.model.UploadStatus;

import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = "ufp.jobs.enabled=false")
@Transactional(transactionManager = "ufpTransactionManager")
public class UploadRepositoryTest {

    @Autowired
    private UploadRepository uploadRepository;

    @Test
    public void findByStatusAndDeletedTest() {
        Page<UfpUpload> uploads = uploadRepository.findByStatusAndDeleted(UploadStatus.NEW, Boolean.FALSE, PageRequest.of(1, 5));

        assertThat(uploads.get().collect(Collectors.toList()), not(IsEmptyCollection.empty()));
        assertNotNull(uploads.get().collect(Collectors.toList()).get(0).getAction());
    }

    @Test
    public void findByStatusAndActionAndDeletedTest() {
        Page<UfpUpload> uploads = uploadRepository.findByStatusAndActionNameAndDeleted(UploadStatus.FAILED, ActionName.UPLOAD_TO_SFTP, Boolean.FALSE, PageRequest.of(1, 5));

        assertThat(uploads.get().collect(Collectors.toList()), not(IsEmptyCollection.empty()));
        assertNotNull(uploads.get().collect(Collectors.toList()).get(0).getAction());
    }

    @Test
    public void updateUploadStatusTest() {
        uploadRepository.updateUploadStatus(UploadStatus.PROCESSING, 1L);

        UfpUpload upload = uploadRepository.findById(1L).orElse(null);

        assertNotNull(upload);
    }


}
