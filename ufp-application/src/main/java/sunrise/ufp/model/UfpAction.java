package sunrise.ufp.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Data
@Entity
@Table(name = "UFP_ACTION")
public class UfpAction implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", updatable = false, nullable = false)
    private Long id;

    @Column(name = "NAME", nullable = false)
    @Enumerated(EnumType.STRING)
    private ActionName name;

    @OneToMany(mappedBy = "ufpAction", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<UfpExecution> executions;

    @Column(name = "RETRY_TIMEOUT", nullable = false)
    private int retryTimeout;

    @Column(name = "RETRY_ATTEMPTS", nullable = false)
    private int retryAttempts;

    @Column(name = "ENABLED", nullable = false)
    private boolean enabled;

    public void addUfpExecution(UfpExecution execution) {
        this.executions.add(execution);
        execution.setUfpAction(this);
    }

    public void removeUfpExecution(UfpExecution execution) {
        this.executions.remove(execution);
        execution.setUfpAction(null);
    }
}
