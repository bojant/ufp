package sunrise.ufp.model;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@Entity
@ToString(exclude = "files")
@Table(name = "UFP_UPLOAD")
public class UfpUpload implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", updatable = false, nullable = false)
    private Long id;

    @OneToMany(mappedBy = "ufpUpload", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<UfpFile> files = new ArrayList<>();

    @Column(name = "CONFIRMATION_ID", nullable = false)
    private String confirmationId;

    @Column(name = "USER_ID", nullable = false)
    private Long userId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ACTION_ID", referencedColumnName = "ID", nullable = false)
    private UfpAction action;

    @Column(name = "UPLOAD_AT", nullable = false)
    private Date uploadAt;

    @Column(name = "STATUS", nullable = false)
    @Enumerated(EnumType.STRING)
    private UploadStatus status;

    @Column(name = "DELETED", nullable = false)
    private boolean deleted;

    public void addUfpFile(UfpFile file) {
        this.files.add(file);
        file.setUfpUpload(this);
    }

    public void removeUfpFile(UfpFile file) {
        this.files.remove(file);
        file.setUfpUpload(null);
    }
}
