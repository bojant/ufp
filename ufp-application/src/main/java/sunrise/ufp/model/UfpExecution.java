package sunrise.ufp.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Getter
@Entity
@Table(name = "UFP_EXECUTION")
public class UfpExecution implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", updatable = false, nullable = false)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FILE_ID", referencedColumnName = "ID", nullable = false)
    private UfpFile ufpFile;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ACTION_ID", referencedColumnName = "ID", nullable = false)
    private UfpAction ufpAction;

    @Column(name = "DATE")
    private Date date;

    @Column(name = "STATUS", nullable = false)
    @Enumerated(EnumType.STRING)
    private FileProcessingStatus status;

    @Column(name = "TRY_COUNTER")
    private int tryCounter;

    @Column(name = "ERROR_MESSAGE")
    private String errorMessage;

    @Column(name = "MANUAL")
    private boolean manualRetry;
}
