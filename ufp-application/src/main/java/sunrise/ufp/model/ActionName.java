package sunrise.ufp.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum ActionName {

    UPLOAD_TO_SFTP("UPLOAD_TO_SFTP");

    private String value;

    ActionName(String value){
        this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
        return String.valueOf(value);
    }

    @JsonCreator
    public static ActionName fromValue(String value) {
        for (ActionName type : ActionName.values()) {
            if (type.value.equals(value)) {
                return type;
            }
        }

        throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }
}
