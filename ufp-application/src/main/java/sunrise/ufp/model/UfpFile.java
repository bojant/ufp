package sunrise.ufp.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Blob;
import java.util.List;

@Data
@Entity
@Table(name = "UFP_FILE")
public class UfpFile implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", updatable = false, nullable = false)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "UPLOAD_ID", referencedColumnName = "ID", nullable = false)
    private UfpUpload ufpUpload;

    @OneToMany(mappedBy = "ufpFile", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<UfpExecution> executions;

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "FILE_TYPE", nullable = false)
    private String fileType;

    @Column(name = "CONTENT", nullable = false)
    private Blob content;

    //TODO check is this needed
    @Column(name = "PROCESSING_STATUS", nullable = false)
    @Enumerated(EnumType.STRING)
    private FileProcessingStatus processingStatus;

    public void addUfpExecution(UfpExecution execution) {
        this.executions.add(execution);
        execution.setUfpFile(this);
    }

    public void removeUfpExecution(UfpExecution execution) {
        this.executions.remove(execution);
        execution.setUfpFile(null);
    }
}
