package sunrise.ufp.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum FileProcessingStatus {
    SUCCESS("SUCCESS"),
    FAIL("FAIL");

    private String value;

    FileProcessingStatus(String value) {
        this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
        return String.valueOf(value);
    }

    @JsonCreator
    public static FileProcessingStatus fromValue(String value) {
        for (FileProcessingStatus b : FileProcessingStatus.values()) {
            if (b.value.equals(value)) {
                return b;
            }
        }
        throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }
}
