package sunrise.ufp.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum UploadStatus {
    NEW ("NEW"),
    PROCESSING ("PROCESSING"),
    PROCESSED ("PROCESSED"),
    FAILED ("FAILED");

    private String value;

    UploadStatus(String value){
        this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
        return String.valueOf(value);
    }

    @JsonCreator
    public static UploadStatus fromValue(String value) {
        for (UploadStatus b : UploadStatus.values()) {
            if (b.value.equals(value)) {
                return b;
            }
        }
        throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }
}
