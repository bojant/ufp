package sunrise.ufp.configuration;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.util.ClassUtils;
import sunrise.ufp.model.UfpDomainRoot;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@EnableAutoConfiguration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "ufpEntityManagerFactory", transactionManagerRef = "ufpTransactionManager", basePackages = {"sunrise.ufp.repository"})
public class DatabaseConfig {

    private DataSource ufpDataSource;
    private PlatformTransactionManager ufpTransactionManager;

    @Bean(name = "ufpDataSource")
    @Primary
    @ConfigurationProperties(prefix = "spring.datasource")
    public DataSource ufpDataSource() {
        if (ufpDataSource == null) {
            this.ufpDataSource = DataSourceBuilder.create().build();
        }

        return ufpDataSource;
    }

    @Bean(name = "ufpTransactionManager")
    @Primary
    public PlatformTransactionManager ufpTransactionManager(@Qualifier("ufpEntityManagerFactory") EntityManagerFactory ufpEntityManagerFactory) {
        if (ufpTransactionManager == null) {
            this.ufpTransactionManager = new JpaTransactionManager(ufpEntityManagerFactory);
        }

        return ufpTransactionManager;
    }

    @Bean(name = "ufpJdbcTemplate")
    @Primary
    public JdbcTemplate ufpJdbcTemplate() {
        return new JdbcTemplate(ufpDataSource());
    }

    @Bean(name = "ufpEntityManagerFactory")
    @Primary
    LocalContainerEntityManagerFactoryBean ufpEntityManagerFactory() {

        HibernateJpaVendorAdapter jpaVendorAdapter = new HibernateJpaVendorAdapter();
        jpaVendorAdapter.setGenerateDdl(false);
        jpaVendorAdapter.setDatabase(Database.ORACLE);

        LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();

        factoryBean.setDataSource(ufpDataSource());
        factoryBean.setJpaVendorAdapter(jpaVendorAdapter);
        factoryBean.setPackagesToScan(ClassUtils.getPackageName(UfpDomainRoot.class));

        return factoryBean;
    }

}
