package sunrise.ufp.job;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.configuration.annotation.BatchConfigurer;
import org.springframework.batch.core.configuration.annotation.DefaultBatchConfigurer;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.lang.NonNull;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import sunrise.ufp.repository.UploadRepository;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.atomic.AtomicInteger;

@EnableBatchProcessing
@Slf4j
public class UploadJobConfig {
    @Autowired
    @Qualifier("ufpTransactionManager")
    PlatformTransactionManager transactionManager;

    @Autowired
    @Qualifier("ufpEntityManagerFactory")
    LocalContainerEntityManagerFactoryBean ufpEntityManagerFactory;

    @Autowired
    UploadRepository uploadRepository;

    @Value("${ufp.feature-enabled}")
    boolean ufpEnabled;

    AtomicInteger batchRunCounter = new AtomicInteger(0);

    JpaTransactionManager jpaTransactionManager;

    @Bean
    public BatchConfigurer batchConfigurer() {
        return new DefaultBatchConfigurer() {

            @Override
            @NonNull
            public PlatformTransactionManager getTransactionManager() {
                if (jpaTransactionManager == null) {
                    jpaTransactionManager = new JpaTransactionManager();
                    jpaTransactionManager.setEntityManagerFactory(ufpEntityManagerFactory.getObject());
                }

                return jpaTransactionManager;
            }
        };
    }

    void lunchTheJob(Job job, JobLauncher jobLauncher) throws JobParametersInvalidException, JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException {
        if (ufpEnabled) {
            DateTimeFormatter formatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME.withZone(ZoneId.systemDefault());

            Instant date = Instant.now();
            log.debug(String.format("Batch job [%s] started at: [%s]", job.getName(), date));
            JobExecution jobExecution = jobLauncher
                    .run(job, new JobParametersBuilder().addString("launchDate", formatter.format(date))
                            .toJobParameters());
            batchRunCounter.incrementAndGet();

            date = Instant.now();
            log.debug(String.format("Batch job [%s] execution [#%d] ends at [%s] with status [%s]", job.getName(),
                    batchRunCounter.get(), formatter.format(date), jobExecution.getStatus()));
        }
    }
}
