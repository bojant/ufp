package sunrise.ufp.job;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.repository.support.JobRepositoryFactoryBean;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.data.RepositoryItemReader;
import org.springframework.batch.item.data.RepositoryItemWriter;
import org.springframework.batch.item.data.builder.RepositoryItemReaderBuilder;
import org.springframework.batch.item.data.builder.RepositoryItemWriterBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import sunrise.ufp.model.UfpUpload;
import sunrise.ufp.model.UploadStatus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Configuration
@EnableBatchProcessing
@EnableScheduling
@Slf4j
public class UploadLeaderJobConfig extends UploadJobConfig {

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Value("${ufp.jobs.leader.enabled}")
    private boolean jobEnabled;

    @Value("${ufp.jobs.leader.pool.processing-chunk-size}")
    private int jobProcessingChunkSize;

    @Bean(name = "leaderJobLauncher")
    public JobLauncher jobLauncher() throws Exception {
        SimpleJobLauncher jobLauncher = new SimpleJobLauncher();
        jobLauncher.setJobRepository(jobRepository());
        jobLauncher.afterPropertiesSet();

        return jobLauncher;
    }

    private JobRepository jobRepository() throws Exception {
        JobRepositoryFactoryBean factory = new JobRepositoryFactoryBean();
        factory.setDataSource(ufpEntityManagerFactory.getDataSource());
        factory.setTransactionManager(transactionManager);
        factory.afterPropertiesSet();

        return factory.getObject();
    }

    @Scheduled(fixedRateString = "${ufp.jobs.leader.pool.time-interval}")
    public void launchJob() throws Exception {
        if (jobEnabled) {
            lunchTheJob(processingJob(), jobLauncher());
        }
    }

    @Bean(name = "leaderProcessingJob")
    public Job processingJob() {
        return jobBuilderFactory.get("leaderProcessingJob")
                .incrementer(new RunIdIncrementer())
                .flow(pollUploadStep())
                .end()
                .build();
    }

    @Bean(name = "leaderPoolUploadsStep")
    public Step pollUploadStep() {
        return stepBuilderFactory.get("leaderPoolUploadsStep").<UfpUpload, UfpUpload>chunk(jobProcessingChunkSize)
                .reader(fetchUploadsReader())
                .processor(uploadProcessor())
                .writer(updateUploadsWriter())
                .faultTolerant()
                .skip(Exception.class)
                .build();
    }

    /**
     * Fetch all Uploads with related entities (File, Action....)
     *
     * @return RepositoryItemReaderBuilder for Uploads
     */
    @Bean(name = "leaderReader")
    public RepositoryItemReader<UfpUpload> fetchUploadsReader() {
        Map<String, Sort.Direction> sort = new HashMap<>();
        sort.put("id", Sort.Direction.ASC);

        List<Object> arguments = new ArrayList<>();
        arguments.add(UploadStatus.NEW);
        arguments.add(Boolean.FALSE);

        return new RepositoryItemReaderBuilder<UfpUpload>()
                .name("leaderReader")
                .repository(uploadRepository)
                .methodName("findByStatusAndDeleted")
                .arguments(arguments)
                .sorts(sort)
                .build();
    }

    @Bean(name = "leaderProcessor")
    public ItemProcessor<UfpUpload, UfpUpload> uploadProcessor() {
        return new UploadItemProcessor();
    }

    @Bean(name = "leaderWriter")
    public RepositoryItemWriter<UfpUpload> updateUploadsWriter() {
        return new RepositoryItemWriterBuilder<UfpUpload>()
                .repository(uploadRepository)
                .methodName("save")
                .build();
    }
}
