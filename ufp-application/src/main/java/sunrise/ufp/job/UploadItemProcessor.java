package sunrise.ufp.job;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.transaction.annotation.Transactional;
import sunrise.ufp.SftpService;
import sunrise.ufp.exception.UfpException;
import sunrise.ufp.model.*;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.time.Instant;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Slf4j
@Transactional(transactionManager = "ufpTransactionManager")
public class UploadItemProcessor implements ItemProcessor<UfpUpload, UfpUpload> {

    @Autowired
    private SftpService sftpService;

    @Value("${ufp.temp-folder}")
    private String tempFolder;

    @Autowired
    private ApplicationContext context;

    @Override
    public UfpUpload process(UfpUpload upload) throws UfpException {
        try {
            boolean successExecution = false;
            ActionName actionName = upload.getAction().getName();

            if (ActionName.UPLOAD_TO_SFTP == actionName) {
                log.debug(String.format("%s action called", actionName.name()));

                List<UfpFile> fileList = upload.getFiles();

                if (CollectionUtils.isEmpty(fileList)) {
                    throw new UfpException("No valid upload files to transfer to SFTP");
                }

                for (UfpFile file : fileList) {
                    Optional<UfpExecution> max = file.getExecutions().stream().max(Comparator.comparing(UfpExecution::getTryCounter));
                    int maxTryCount = max.map(UfpExecution::getTryCounter).orElse(0);
                    UfpExecution execution = UfpExecution.builder()
                            .ufpFile(file)
                            .ufpAction(upload.getAction())
                            .date(Date.from(Instant.now()))
                            .tryCounter(++maxTryCount)
                            .manualRetry(Boolean.FALSE)
                            .build();

                    successExecution = sftpService.uploadFile(convertBlobToFile(file));

                    if (successExecution) {
                        file.setProcessingStatus(FileProcessingStatus.SUCCESS);
                        execution = execution.toBuilder()
                                .status(FileProcessingStatus.SUCCESS)
                                .build();

                        // update file
                        file.addUfpExecution(execution);
                    } else {
                        file.setProcessingStatus(FileProcessingStatus.FAIL);
                        execution =  execution.toBuilder()
                                .status(FileProcessingStatus.FAIL)
                                .errorMessage("ERROR while transfer file to SFTP")
                                .build();

                        // update file
                        file.addUfpExecution(execution);

                        // if one of the upload file action fails, update statuses and break upload process
                        break;
                    }
                }

                if (successExecution) {
                    upload.setStatus(UploadStatus.PROCESSED);
                } else {
                    upload.setStatus(UploadStatus.FAILED);
                }

            } else {
                throw new UfpException(upload.getAction().getName() + " action not supported.");
            }

            // clean temp folder
            emptyTempFolder();

        } catch (UfpException e) {
            log.error(String.format("Failed to process upload with id: %s and action: %s", upload.getId(), upload.getAction().getName()), e);
            upload.setStatus(UploadStatus.FAILED);
        }

        return upload;
    }

    File convertBlobToFile(UfpFile ufpFile) {
        String fileName = ufpFile.getName() + "." + ufpFile.getFileType();
        File file = new File(tempFolder + fileName);

        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] buf = new byte[1024];

            InputStream in = ufpFile.getContent().getBinaryStream();

            int n = 0;
            while ((n = in.read(buf)) >= 0) {
                baos.write(buf, 0, n);
            }

            in.close();
            byte[] bytes = baos.toByteArray();

            FileUtils.writeByteArrayToFile(file, bytes);
        } catch (SQLException | IOException e) {
            log.error(String.format("Error while generating the file: %s", fileName), e);
        }

        return file;
    }

    void emptyTempFolder() {
        try {
            FileUtils.cleanDirectory(new File(tempFolder));
        } catch (IOException e) {
            log.debug(String.format("Error deleting temp folder %s: ", tempFolder), e);
        }
    }
}
