package sunrise.ufp.job;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.repository.support.JobRepositoryFactoryBean;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.data.RepositoryItemReader;
import org.springframework.batch.item.data.RepositoryItemWriter;
import org.springframework.batch.item.data.builder.RepositoryItemReaderBuilder;
import org.springframework.batch.item.data.builder.RepositoryItemWriterBuilder;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Sort;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.scheduling.annotation.Scheduled;
import sunrise.ufp.model.ActionName;
import sunrise.ufp.model.UfpAction;
import sunrise.ufp.model.UfpUpload;
import sunrise.ufp.model.UploadStatus;
import sunrise.ufp.repository.ActionRepository;

import java.util.*;

@Configuration
@EnableBatchProcessing
@EnableRetry
@Slf4j
public class UploadToSftpRetryJobConfig extends UploadJobConfig {

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    private ActionRepository actionRepository;

    @Value("${ufp.jobs.upload-to-sftp.pool.processing-chunk-size}")
    private int jobProcessingChunkSize;

    @Value("${ufp.jobs.upload-to-sftp.enabled}")
    private boolean jobEnabled;

    //default action parameters
    private int actionRetryAttempts = 5;
    private int actionRetryTimeout = 50000;

    @Bean(name = "uploadToSftpRetryJobLauncher")
    public JobLauncher jobLauncher() throws Exception {
        SimpleJobLauncher jobLauncher = new SimpleJobLauncher();
        jobLauncher.setJobRepository(jobRepository());
        jobLauncher.afterPropertiesSet();

        return jobLauncher;
    }

    private JobRepository jobRepository() throws Exception {
        JobRepositoryFactoryBean factory = new JobRepositoryFactoryBean();
        factory.setDataSource(ufpEntityManagerFactory.getDataSource());
        factory.setTransactionManager(transactionManager);
        factory.afterPropertiesSet();

        return factory.getObject();
    }

    @Scheduled(fixedRateString = "${ufp.jobs.upload-to-sftp.pool.time-interval}")
    public void launchJob() throws Exception {
        if (jobEnabled) {
            lunchTheJob(processingJob(), jobLauncher());
        }
    }

    @Bean(name = "uploadToSftpRetryProcessingJob")
    public Job processingJob() {
        return jobBuilderFactory.get("uploadToSftpRetryProcessingJob")
                .incrementer(new RunIdIncrementer())
//                .start(fetchActionParametersStep())
                .flow(processingStep())
                .end()
                .build();
    }

    @Bean(name = "uploadToSftpRetryProcessingStep")
    public Step processingStep() {
        return stepBuilderFactory.get("uploadToSftpRetryProcessingStep").<UfpUpload, UfpUpload>chunk(jobProcessingChunkSize)
                .reader(fetchUploadsReader())
                .processor(uploadProcessor())
                .writer(updateUploadsWriter())
                .build();
    }

    /**
     * Fetch all Uploads with related entities (File, Action....)
     *
     * @return RepositoryItemReaderBuilder for Uploads
     */
    @Bean(name = "uploadToSftpRetryReader")
    public RepositoryItemReader<UfpUpload> fetchUploadsReader() {
        Map<String, Sort.Direction> sort = new HashMap<>();
        sort.put("id", Sort.Direction.ASC);

        List<Object> arguments = new ArrayList<>();
        arguments.add(UploadStatus.FAILED);
        arguments.add(ActionName.UPLOAD_TO_SFTP);
        arguments.add(Boolean.FALSE);

        return new RepositoryItemReaderBuilder<UfpUpload>()
                .name("uploadToSftpRetryReader")
                .repository(uploadRepository)
                .methodName("findByStatusAndActionNameAndDeleted")
                .arguments(arguments)
                .sorts(sort)
                .build();
    }

    @Bean(name = "uploadToSftpRetryProcessor")
    public ItemProcessor<UfpUpload, UfpUpload> uploadProcessor() {
        return new UploadItemProcessor();
    }

    @Bean(name = "uploadToSftpRetryWriter")
    public RepositoryItemWriter<UfpUpload> updateUploadsWriter() {
        return new RepositoryItemWriterBuilder<UfpUpload>()
                .repository(uploadRepository)
                .methodName("save")
                .build();
    }

    @Bean
    public Step fetchActionParametersStep() {
        return stepBuilderFactory.get("fetchActionParametersStep")
                .tasklet(fetchActionParametersTasklet())
                .build();
    }

    @Bean
    public Tasklet fetchActionParametersTasklet() {
        Optional<UfpAction> action = actionRepository.findByName(ActionName.UPLOAD_TO_SFTP);

        if (action.isPresent()) {
            actionRetryAttempts = action.get().getRetryAttempts();
            actionRetryTimeout = action.get().getRetryTimeout();
        } else {
            log.debug(String.format("Unable to fetch retry parameters for action %s, default will be used: retry_attempts = %s, retry_timeout = %s",
                    ActionName.UPLOAD_TO_SFTP.name(), actionRetryAttempts, actionRetryTimeout));
        }

        return ((contribution, chunkContext) -> RepeatStatus.FINISHED);
    }

//    private FixedBackOffPolicy fixedBackOffPolicy() {
//        FixedBackOffPolicy fixedBackOffPolicy = new FixedBackOffPolicy();
//        fixedBackOffPolicy.setBackOffPeriod(actionRetryTimeout);
//        return fixedBackOffPolicy;
//    }
//
//    private SimpleRetryPolicy simpleRetryPolicy() {
//        SimpleRetryPolicy simpleRetryPolicy = new SimpleRetryPolicy();
//        simpleRetryPolicy.setMaxAttempts(actionRetryAttempts);
//
//        return simpleRetryPolicy;
//    }
}
