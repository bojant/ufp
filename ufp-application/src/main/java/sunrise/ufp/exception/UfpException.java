package sunrise.ufp.exception;

public class UfpException extends Exception {

    public UfpException(String message) {
        super(message);
    }

    public UfpException(String message, Throwable cause) {
        super(message, cause);
    }
}
