package sunrise.ufp.logging;

import org.slf4j.MDC;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component(value = "httpLoggingFilter")
public class HttpLoggingFilter implements Filter {

    public static final String CONTEXT_ID = "ContextId";
    public static final String REQUEST_ID = "RequestId";

    private static final String CORRELATION_ID_HEADER = "X-Correlation-ID";
    private static final String REQUEST_ID_HEADER = "X-Request-ID";

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        process((HttpServletRequest) request, (HttpServletResponse) response, CORRELATION_ID_HEADER, CONTEXT_ID);
        process((HttpServletRequest) request, (HttpServletResponse) response, REQUEST_ID_HEADER, REQUEST_ID);
        chain.doFilter(request, response);
    }

    @Override
    public void init(FilterConfig config) throws ServletException {}

    @Override
    public void destroy() {}

    private void process(HttpServletRequest request, HttpServletResponse response, String headerName, String mdcName) {
        String headerValue = request.getHeader(headerName);
        MDC.put(mdcName, headerValue);
        response.setHeader(headerName, headerValue);
    }
}
