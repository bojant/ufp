package sunrise.ufp.logging;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;

import javax.servlet.Filter;

@Configuration
public class LoggingConfiguration {

    @Autowired
    @Qualifier("httpLoggingFilter")
    private Filter httpHeaderFilter;

    @Bean
    public FilterRegistrationBean<Filter> headersFilterRegistration() {
        final FilterRegistrationBean<Filter> filterRegistrationBean = new FilterRegistrationBean<>();
        filterRegistrationBean.setFilter(httpHeaderFilter);
        filterRegistrationBean.addUrlPatterns("/*");
        filterRegistrationBean.setName("HttpLoggingFilter");
        filterRegistrationBean.setOrder(Ordered.HIGHEST_PRECEDENCE);
        return filterRegistrationBean;
    }
}
