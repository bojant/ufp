package sunrise.ufp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import sunrise.ufp.model.UfpExecution;

@Repository
@Transactional
public interface ExecutionRepository extends JpaRepository<UfpExecution, Long> {
}
