package sunrise.ufp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import sunrise.ufp.model.ActionName;
import sunrise.ufp.model.UfpAction;

import java.util.Optional;

@Repository
@Transactional
public interface ActionRepository extends JpaRepository<UfpAction, Long> {

    Optional<UfpAction> findByName(ActionName name);
}
