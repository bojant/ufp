package sunrise.ufp.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import sunrise.ufp.model.ActionName;
import sunrise.ufp.model.UfpAction;
import sunrise.ufp.model.UfpUpload;
import sunrise.ufp.model.UploadStatus;

@Repository
public interface UploadRepository extends JpaRepository<UfpUpload, Long> {

    Page<UfpUpload> findByStatusAndDeleted(UploadStatus status, Boolean deleted, Pageable pageable);

    Page<UfpUpload> findByStatusAndActionNameAndDeleted(UploadStatus status, ActionName action, Boolean deleted, Pageable pageable);

    @Query("select u from UfpUpload u where u.status = :status and u.deleted = :deleted and u.action.name = :action and" +
            "u.execution")
    Page<UfpUpload> findUploadsForRetry(@Param("status") String status, @Param("action") String action, @Param("deleted") Boolean deleted, Pageable pageable);


    @Modifying
    @Query("update UfpUpload set status = :status where id = :id")
    void updateUploadStatus(@Param("status") UploadStatus status, @Param("id") Long id);
}
