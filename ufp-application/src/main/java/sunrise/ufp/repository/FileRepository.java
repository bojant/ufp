package sunrise.ufp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import sunrise.ufp.model.UfpFile;

@Repository
@Transactional
public interface FileRepository extends JpaRepository<UfpFile, Long> {
}
