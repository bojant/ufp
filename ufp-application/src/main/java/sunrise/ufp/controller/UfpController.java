package sunrise.ufp.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.SwaggerDefinition;
import io.swagger.annotations.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import sunrise.ufp.model.UploadDto;

@RestController
@Slf4j
@Api(tags = {"UFP"})
@SwaggerDefinition(tags = {@Tag(name = "UFP Resource", description = "Set of UFP endpoints")})
public class UfpController implements UfpApi {

    @Override
    public ResponseEntity<UploadDto> getAllUploads() {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }
}
