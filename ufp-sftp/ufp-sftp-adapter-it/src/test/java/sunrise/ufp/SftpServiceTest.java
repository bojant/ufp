package sunrise.ufp;

import org.hamcrest.collection.IsEmptyCollection;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { sunrise.ufp.SftpLocalConfig.class, sunrise.ufp.LocalFilesystemService.class } )
@EnableAutoConfiguration
@ComponentScan
@PropertySource(value = "classpath:application-test.yml", encoding = "UTF-8")
public class SftpServiceTest {

//    @Value("${sftp.upload.folder}")
    private String defaultFolder = "c://dev//sandbox//ufp//upload//";

    @Autowired
    private SftpService sftpService;

    @Test
    public void getAllFilesTest() {
        List<String> allFiles = sftpService.getAllFiles(defaultFolder);
        assertThat(allFiles, not(IsEmptyCollection.empty()));
    }
}

