package sunrise.ufp;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

@Configuration
@Profile("sftp")
@PropertySource(value = "classpath:/sftp.properties")
@EnableConfigurationProperties(SftpProperties.class)
public class SftpConfig {
}
