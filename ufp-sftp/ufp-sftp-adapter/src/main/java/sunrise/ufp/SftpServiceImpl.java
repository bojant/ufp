package sunrise.ufp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.List;

@Component
@Profile("sftp")
public class SftpServiceImpl implements SftpService{

    @Autowired
    private SftpProperties properties;
//
//    @Value("${sftp.upload.folder}")
//    private String defaultFolder;

    @Autowired
    private SftpClient sftpClient;

    @Autowired
    SftpConfig sftpConfig;

    @Override
    public List<String> getAllFiles(String folderName) {
        return sftpClient.get(folderName);
    }

    @Override
    public byte[] downloadFileByName(String fileName) {
        return sftpClient.download(properties.getUploadFolder(), fileName);
    }

    @Override
    public byte[] downloadFileByName(String folderName, String fileName) {
        return sftpClient.download(folderName, fileName);
    }

    @Override
    public boolean uploadFile(File file) {
        return sftpClient.upload(file);
    }

    @Override
    public boolean uploadFile(String location, File file) {
        return sftpClient.upload(location, file);
    }
}
