package sunrise.ufp;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;

@Data
@Validated
@ConfigurationProperties(prefix = "sftp")
public class SftpProperties {
    @NotBlank
    private String host;

    @NotBlank
    private String port;

    @NotBlank
    private String user;

    @NotBlank
    private String pass;

    @NotBlank
    private String uploadFolder;

}
