package sunrise.ufp;

import com.jcraft.jsch.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;

@Component
@Slf4j
@Profile("sftp")
public class SftpClient {

    @Autowired
    private SftpProperties properties;

    private ChannelSftp channelSftp;
    private Session session;

    @SuppressWarnings("unchecked")
    public List<String> get(String folderName) {
        List<String> files = new ArrayList<>();
        try {
            connect();
            Collection<ChannelSftp.LsEntry> fileVector = channelSftp.ls(folderName);
            for (ChannelSftp.LsEntry lsEntry : fileVector) {
                if (!lsEntry.getFilename().equals(".") && !lsEntry.getFilename().equals("..")) {
                    files.add(lsEntry.getFilename());
                }
            }
        } catch (SftpException e) {
            log.error(String.format("Sftp error while fetching files from folder %s", folderName), e);
        } catch (JSchException e) {
            log.error("Filed to connect to SFTP", e);
        } finally {
            disconnect();
        }
        return files;
    }

    public boolean upload(File file) {
        try (FileInputStream input = new FileInputStream(file)) {
            connect();
            channelSftp.cd(properties.getUploadFolder());
            channelSftp.put(input, file.getName(), ChannelSftp.OVERWRITE);
            return true;
        } catch (SftpException e) {
            log.error(String.format("Sftp error while uploading file %s", file.getName()), e);
        } catch (IOException e) {
            log.error("IO error", e);
        } catch (JSchException e) {
            log.error("Filed to connect to SFTP", e);
        } finally {
            disconnect();
        }

        return false;
    }

    public boolean upload(String location, File file) {
        try (FileInputStream input = new FileInputStream(file)) {
            connect();
            channelSftp.cd(location);
            channelSftp.put(input, file.getName(), ChannelSftp.OVERWRITE);
            return true;
        } catch (SftpException e) {
            log.error(String.format("Sftp error while uploading file %s", file.getName()), e);
        } catch (IOException e) {
            log.error("IO error", e);
        } catch (JSchException e) {
            log.error("Filed to connect to SFTP", e);
        } finally {
            disconnect();
        }
        return false;
    }

    public byte[] download(String folderName, String filename) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try (BufferedOutputStream buff = new BufferedOutputStream(outputStream)) {
            connect();
            channelSftp.cd(folderName);
            channelSftp.get(filename, buff);
            return outputStream.toByteArray();
        } catch (SftpException e) {
            log.error(String.format("Sftp error while downloading file %s from folder %s", filename, folderName), e);
            log.error("Sftp error", e);
        } catch (IOException e) {
            log.error("IO error", e);
        } catch (JSchException e) {
            log.error("Filed to connect to SFTP", e);
        } finally {
            disconnect();
        }

        return ArrayUtils.EMPTY_BYTE_ARRAY;
    }

    private void connect() throws JSchException {
        JSch jSch = new JSch();

        session = jSch.getSession(properties.getUser(), properties.getHost(), Integer.parseInt(properties.getPort()));
        session.setPassword(properties.getPass());
        Properties config = new Properties();
        config.put("StrictHostKeyChecking", "no");
        session.setConfig(config);
        session.connect();
        channelSftp = (ChannelSftp) session.openChannel("sftp");
        channelSftp.connect();

    }

    private void disconnect() {
        if (channelSftp != null) {
            channelSftp.disconnect();
        }
        if (session != null) {
            session.disconnect();
        }
    }

}
