package sunrise.ufp;

import java.io.File;
import java.util.List;

public interface SftpService {

    List<String> getAllFiles(String folderName);

    byte[] downloadFileByName(String fileName);

    byte[] downloadFileByName(String folderName, String fileName);

    boolean uploadFile(File file);

    boolean uploadFile(String location, File file);
}
