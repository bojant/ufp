package sunrise.ufp;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.util.FileCopyUtils;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Slf4j
@Profile("!sftp")
public class LocalFilesystemService implements SftpService {

    @Autowired
    private SftpLocalProperties properties;

    @PostConstruct
    public void createFolder() {
        File uploadFolder = new File(properties.getUploadFolder());
        if (!uploadFolder.exists()) {
            uploadFolder.mkdirs();
        }

        /* // not used
        File downloadFolder = new File(properties.getUploadFolder());
        if (!downloadFolder.exists()) {
            downloadFolder.mkdirs();
        }
        */
    }

    @Override
    public List<String> getAllFiles(String folderName) {
        File uploadFolder = new File(folderName);
        File[] files = uploadFolder.listFiles();

        if (ArrayUtils.isNotEmpty(files)) {
            List<File> fileList = Arrays.asList(files);
            return fileList.stream().map(File::getName).collect(Collectors.toList());
        }

        return Collections.emptyList();
    }

    @Override
    public byte[] downloadFileByName(String fileName) {
        return this.downloadFileByName(properties.getUploadFolder(), fileName);
    }

    @Override
    public byte[] downloadFileByName(String folderName, String fileName) {
        File file = new File(folderName.concat("/").concat(fileName));

        if (file.exists()) {
            try {
                return FileCopyUtils.copyToByteArray(file);
            } catch (IOException e) {
                log.error("File doesn't exists: " + fileName, e);
            }
        }

        return ArrayUtils.EMPTY_BYTE_ARRAY;
    }

    @Override
    public boolean uploadFile(File file) {
        return this.uploadFile(properties.getUploadFolder(), file);
    }

    @Override
    public boolean uploadFile(String destination, File file) {
        File dest = new File(destination + file.getName());

        try {
            Files.copy(file.toPath(), dest.toPath(), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            log.error("Error while saving file: " + file.getName(), e);
            return false;
        }

        return true;
    }
}
